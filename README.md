# MAN FOTA Coding Challenge

When installing software over-the-air in trucks, one of the most relevant tasks is to know which functionalities are compatible with the truck's configuration (software and hardware).

In this exercise, the purpose is to create a service that will solve this challenge.

## Setup:

Unzip the file in a folder of your choice.

Before starting, please go to the `application.properties`, located in `/src/main/resources`, and change the value of property

`configurations.file.path` from *XXX* to a path of your choosing.

## Running

In the root folder (`challenge`) run the following command:

$ ./gradlew build && java -jar build/libs/challenge-0.0.1-SNAPHOST.jar

## Notes

Commits history available at https://bitbucket.org/ritacpereira/challenge/src/master/