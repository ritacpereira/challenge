package com.man.fota.challenge.controllers;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping(path = "/")
public class HomeController implements ErrorController {

    @RequestMapping(path = "/error", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] handleError() throws IOException {
        final InputStream stream = getClass().getResourceAsStream("/images/csn.jpg");
        return IOUtils.toByteArray(stream);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}
