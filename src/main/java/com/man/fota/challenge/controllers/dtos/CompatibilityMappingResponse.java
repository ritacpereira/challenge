package com.man.fota.challenge.controllers.dtos;

import java.util.Collection;

public final class CompatibilityMappingResponse {

    private final Collection<?> installable;
    private final Collection<?> incompatible;

    private CompatibilityMappingResponse(final Collection<?> installable, final Collection<?> incompatible) {
        this.installable = installable;
        this.incompatible = incompatible;
    }

    public static CompatibilityMappingResponse map(final Collection<?> installable, final Collection<?> incompatible) {
        return new CompatibilityMappingResponse(installable, incompatible);
    }

    public Collection<?> getInstallable() {
        return installable;
    }

    public Collection<?> getIncompatible() {
        return incompatible;
    }
}
