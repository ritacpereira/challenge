package com.man.fota.challenge.controllers;

import com.man.fota.challenge.controllers.dtos.CompatibilityMappingResponse;
import com.man.fota.challenge.exceptions.ResourceNotFoundException;
import com.man.fota.challenge.model.Feature;
import com.man.fota.challenge.model.Vehicle;
import com.man.fota.challenge.services.FeatureService;
import com.man.fota.challenge.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(path = FeatureController.FOTA_FEATURES)
public class FeatureController {

    private static final String FEATURE_ERROR_MESSAGE = "Could not find feature %s";

    protected static final String FOTA_FEATURES = "/fota/features";

    private static final String GET = "/";
    private static final String FEATURE = GET + "{feature}";
    private static final String INSTALLABLE = FEATURE + "/installable";
    private static final String INCOMPATIBLE = FEATURE + "/incompatible";

    @Autowired
    private FeatureService featureService;

    @Autowired
    private VehicleService vehicleService;

    @GetMapping(GET)
    public Collection<Feature> getAllFeatures() {
        return featureService.getAll();
    }

    @GetMapping(FEATURE)
    public CompatibilityMappingResponse getAllVin(@PathVariable final String feature) {
        validate(feature);
        return CompatibilityMappingResponse.map(vehicleService.getCompatible(feature), vehicleService.getIncompatible(feature));
    }

    @GetMapping(INSTALLABLE)
    public Collection<Vehicle> getVinInstallable(@PathVariable final String feature) {
        validate(feature);
        return vehicleService.getCompatible(feature);
    }

    @GetMapping(INCOMPATIBLE)
    public Collection<Vehicle> getVinIncompatible(@PathVariable final String feature) {
        validate(feature);
        return vehicleService.getIncompatible(feature);
    }

    private void validate(final String feature) {
        if (featureService.find(feature).isEmpty()) {
            throw new ResourceNotFoundException(String.format(FEATURE_ERROR_MESSAGE, feature));
        }
    }

}
