package com.man.fota.challenge.controllers;

import com.man.fota.challenge.controllers.dtos.CompatibilityMappingResponse;
import com.man.fota.challenge.exceptions.ResourceNotFoundException;
import com.man.fota.challenge.model.Feature;
import com.man.fota.challenge.model.Vehicle;
import com.man.fota.challenge.services.FeatureService;
import com.man.fota.challenge.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(path = VehicleController.FOTA_VEHICLES)
public class VehicleController {

    private static final String VEHICLE_ERROR_MESSAGE = "Could not find vehicle %s";

    protected static final String FOTA_VEHICLES = "/fota/vehicles";

    private static final String GET = "/";
    private static final String VIN = GET + "{vin}";
    private static final String INSTALLABLE = VIN + "/installable";
    private static final String INCOMPATIBLE = VIN + "/incompatible";

    @Autowired
    private FeatureService featureService;

    @Autowired
    private VehicleService vehicleService;

    @GetMapping(GET)
    public Collection<Vehicle> getVehicles() {
        return vehicleService.getAll();
    }

    @GetMapping(VIN)
    public CompatibilityMappingResponse getFeatures(@PathVariable final String vin) {
        validate(vin);
        return CompatibilityMappingResponse.map(featureService.getCompatible(vin), featureService.getIncompatible(vin));
    }

    @GetMapping(INSTALLABLE)
    public Collection<Feature> getInstallable(@PathVariable final String vin) {
        validate(vin);
        return featureService.getCompatible(vin);
    }

    @GetMapping(INCOMPATIBLE)
    public Collection<Feature> getIncompatible(@PathVariable final String vin) {
        validate(vin);
        return featureService.getIncompatible(vin);
    }

    private void validate(final String vehicle) {
        if (vehicleService.find(vehicle).isEmpty()) {
            throw new ResourceNotFoundException(String.format(VEHICLE_ERROR_MESSAGE, vehicle));
        }
    }

}
