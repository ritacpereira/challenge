package com.man.fota.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Immutable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Immutable
@JsonIgnoreProperties(value = {"compatibleVehicles", "incompatibleVehicles"})
public class Feature {

    @Id
    @Column(updatable = false, nullable = false)
    private String id;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "feature_requirement", joinColumns = {
            @JoinColumn(name = "feature_id", referencedColumnName = "id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "requirement_id", referencedColumnName = "id")
    })
    private Set<Configuration> requirements;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "feature_exclusion", joinColumns = {
            @JoinColumn(name = "feature_id", referencedColumnName = "id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "exclusion_id", referencedColumnName = "id")
    })
    private Set<Configuration> exclusions;

    @ManyToMany
    private Set<Vehicle> compatibleVehicles;

    // this is a list of vehicles that will *never* be compatible with this feature,
    // because they have a configuration that is included in the exclusion collection
    @ManyToMany
    private Set<Vehicle> incompatibleVehicles;

    private Feature() {}

    public Feature(final String id) {
        this.id = id;

        this.requirements = new HashSet<>();
        this.exclusions = new HashSet<>();

        this.compatibleVehicles = new HashSet<>();
        this.incompatibleVehicles = new HashSet<>();
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public Collection<Configuration> getRequirements() {
        return Collections.unmodifiableSet(requirements);
    }

    private void setRequirements(Set<Configuration> requirements) {
        this.requirements = requirements;
    }

    public void addRequirement(final Configuration requirement) {
        requirements.add(requirement);
    }

    public Collection<Configuration> getExclusions() {
        return Collections.unmodifiableSet(exclusions);
    }

    private void setExclusions(Set<Configuration> exclusions) {
        this.exclusions = exclusions;
    }

    public void addExclusion(final Configuration exclusion) {
        // assumption:
        // if a configuration is simultaneously in the requirements and configurations collection,
        // it will be added to the requirements only
        if (requirements.contains(exclusion)) {
            return;
        }
        exclusions.add(exclusion);
    }

    public Collection<Vehicle> getCompatibleVehicles() {
        return Collections.unmodifiableSet(compatibleVehicles);
    }

    private void setCompatibleVehicles(Set<Vehicle> compatibleVehicles) {
        this.compatibleVehicles = compatibleVehicles;
    }

    public void addCompatibleVehicle(final Vehicle vehicle) {
        compatibleVehicles.add(vehicle);
    }

    private Collection<Vehicle> getIncompatibleVehicles() {
        return Collections.unmodifiableSet(incompatibleVehicles);
    }

    private void setIncompatibleVehicles(Set<Vehicle> incompatibleVehicles) {
        this.incompatibleVehicles = incompatibleVehicles;
    }

    public void addIncompatibleVehicle(final Vehicle vehicle) {
        compatibleVehicles.remove(vehicle);
        incompatibleVehicles.add(vehicle);
    }

    @Override
    public String toString() {
        return "Feature{" +
                "id='" + id + '\'' +
                ", requirements=" + requirements +
                ", exclusions=" + exclusions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Feature feature = (Feature) o;
        return Objects.equals(id, feature.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
