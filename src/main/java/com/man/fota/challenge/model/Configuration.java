package com.man.fota.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Immutable
@JsonIgnoreProperties(value = {"configurations", "requirements", "exclusions"})
public class Configuration {

    @Id
    @Column(updatable = false, nullable = false)
    private String id;

    private Type type;

    private Configuration() {
    }

    private Configuration(final String id, final Type type) {
        this.id = id;
        this.type = type;
    }

    public static Configuration hardwareConfiguration(final String id) {
        return new Configuration(id, Type.HARDWARE);
    }

    public static Configuration softwareConfiguration(final String id) {
        return new Configuration(id, Type.SOFTWARE);
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    private void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "id='" + id + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Configuration that = (Configuration) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    private enum Type {
        HARDWARE,
        SOFTWARE
    }
}
