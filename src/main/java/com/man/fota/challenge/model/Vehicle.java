package com.man.fota.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@JsonIgnoreProperties(value = {"compatibleFeatures", "incompatibleFeatures"})
public class Vehicle {

    @Id
    @Column(updatable = false, nullable = false)
    private String vin;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "vehicle_configuration", joinColumns = {
            @JoinColumn(name = "vehicle_id", referencedColumnName = "vin")
    }, inverseJoinColumns = {
            @JoinColumn(name = "configuration_id", referencedColumnName = "id")
    })
    private Set<Configuration> configurations;

    @ManyToMany(mappedBy = "compatibleVehicles")
    private Set<Feature> compatibleFeatures;

    // this is a list of features that will *never* be compatible with this vehicle,
    // because this vehicle has some configuration that is included in the exclusion collection
    @ManyToMany(mappedBy = "incompatibleVehicles")
    private Set<Feature> incompatibleFeatures;

    private Vehicle() {}

    public Vehicle(final String vin) {
        this.vin = vin;

        this.configurations = new HashSet<>();
        this.compatibleFeatures = new HashSet<>();
        this.incompatibleFeatures = new HashSet<>();
    }

    public String getVin() {
        return vin;
    }

    private void setVin(String vin) {
        this.vin = vin;
    }

    public Collection<Configuration> getConfigurations() {
        return Collections.unmodifiableSet(configurations);
    }

    private void setConfigurations(Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    public void addConfigurations(final Collection<Configuration> configurations) {
        this.configurations.addAll(configurations);
    }

    public Collection<Feature> getCompatibleFeatures() {
        return Collections.unmodifiableSet(compatibleFeatures);
    }

    private void setCompatibleFeatures(Set<Feature> compatibleFeatures) {
        this.compatibleFeatures = compatibleFeatures;
    }

    public void addCompatibleFeature(final Feature feature) {
        compatibleFeatures.add(feature);
    }

    public Collection<Feature> getIncompatibleFeatures() {
        return Collections.unmodifiableSet(incompatibleFeatures);
    }

    private void setIncompatibleFeatures(Set<Feature> incompatibleFeatures) {
        this.incompatibleFeatures = incompatibleFeatures;
    }

    public void addIncompatibleFeature(final Feature feature) {
        compatibleFeatures.remove(feature);
        incompatibleFeatures.add(feature);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vin='" + vin + '\'' +
                ", configurations=" + configurations +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(vin, vehicle.vin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vin);
    }
}
