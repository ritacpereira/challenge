package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Configuration;
import com.man.fota.challenge.repositories.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;

import static com.man.fota.challenge.model.Configuration.hardwareConfiguration;
import static com.man.fota.challenge.model.Configuration.softwareConfiguration;

@Service
@Transactional
public class ConfigurationService {

    @Autowired
    private ConfigurationRepository repository;

    protected Configuration findOrRegisterSoftwareConfiguration(final String configuration) {
        return findOrRegister(configuration, () -> softwareConfiguration(configuration));
    }

    protected Configuration findOrRegisterHardwareConfiguration(final String configuration) {
        return findOrRegister(configuration, () -> hardwareConfiguration(configuration));
    }

    private Configuration findOrRegister(final String configuration, final Supplier<Configuration> configurationSupplier) {
        return repository.findById(configuration)
                .orElseGet(() -> repository.save(configurationSupplier.get()));
    }

}
