package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Vehicle;
import com.man.fota.challenge.repositories.FeatureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CompatibilityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompatibilityService.class);

    @Autowired
    private FeatureRepository featureRepository;

    protected void processCompatibility(final Vehicle vehicle) {
        featureRepository.findAll().forEach(feature -> {
            if (vehicle.getIncompatibleFeatures().contains(feature)) {
                LOGGER.debug("Bypassing process for feature {} and vehicle {}", feature.getId(), vehicle.getVin());
                return;
            }

            if (vehicle.getConfigurations().stream().anyMatch(configuration -> feature.getExclusions().contains(configuration))) {
                LOGGER.debug("Feature {} and vehicle {} are incompatible", feature.getId(), vehicle.getVin());
                feature.addIncompatibleVehicle(vehicle);
                vehicle.addIncompatibleFeature(feature);

                featureRepository.save(feature);
            } else if (vehicle.getConfigurations().containsAll(feature.getRequirements())) {
                LOGGER.debug("Feature {} and vehicle {} are compatible", feature.getId(), vehicle.getVin());
                feature.addCompatibleVehicle(vehicle);
                vehicle.addCompatibleFeature(feature);

                featureRepository.save(feature);
            }
        });
    }

}
