package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Feature;
import com.man.fota.challenge.model.Vehicle;
import com.man.fota.challenge.repositories.FeatureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class FeatureService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureService.class);

    @Autowired
    private FeatureRepository repository;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CompatibilityService compatibilityService;

    public Collection<Feature> getAll() {
        return repository.findAll();
    }

    public Collection<Feature> getCompatible(final String vin) {
        return vehicleService.find(vin)
                .map(Vehicle::getCompatibleFeatures)
                .orElse(Collections.emptySet());
    }

    public Collection<Feature> getIncompatible(final String vin) {
        final Collection<Feature> compatible = getCompatible(vin);
        return repository.findAll().stream()
                .filter(feature -> !compatible.contains(feature))
                .collect(Collectors.toUnmodifiableSet());
    }

    public void register(final String featureId,
                         final Collection<String> requiredSoftwareCodes, final Collection<String> excludedSoftwareCodes,
                         final Collection<String> requiredHardwareCodes, final Collection<String> excludedHardwareCodes) {
        if (repository.findById(featureId).isPresent()) {
            LOGGER.error("Feature {} already exists", featureId);
            return;
        }

        final Feature feature = register(featureId);

        requiredSoftwareCodes.stream()
                .map(code -> configurationService.findOrRegisterSoftwareConfiguration(code))
                .forEach(feature::addRequirement);
        requiredHardwareCodes.stream()
                .map(code -> configurationService.findOrRegisterHardwareConfiguration(code))
                .forEach(feature::addRequirement);

        excludedSoftwareCodes.stream()
                .map(code -> configurationService.findOrRegisterSoftwareConfiguration(code))
                .forEach(feature::addExclusion);
        excludedHardwareCodes.stream()
                .map(code -> configurationService.findOrRegisterHardwareConfiguration(code))
                .forEach(feature::addExclusion);

        repository.save(feature);
    }

    private Feature register(final String feature) {
        return repository.save(new Feature(feature));
    }

    public Optional<Feature> find(final String feature) {
        return repository.findById(feature);
    }

}
