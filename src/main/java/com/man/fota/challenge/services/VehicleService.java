package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Configuration;
import com.man.fota.challenge.model.Feature;
import com.man.fota.challenge.model.Vehicle;
import com.man.fota.challenge.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class VehicleService {

    @Autowired
    private VehicleRepository repository;

    @Autowired
    private FeatureService featureService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CompatibilityService compatibilityService;

    public Collection<Vehicle> getAll() {
        return repository.findAll();
    }

    public Collection<Vehicle> getCompatible(final String feature) {
        return featureService.find(feature)
                .map(Feature::getCompatibleVehicles)
                .orElse(Collections.emptySet());
    }

    public Collection<Vehicle> getIncompatible(final String feature) {
        final Collection<Vehicle> compatible = getCompatible(feature);
        return repository.findAll().stream()
                .filter(vehicle -> !compatible.contains(vehicle))
                .collect(Collectors.toUnmodifiableSet());
    }

    protected Vehicle register(final String vin) {
        return repository.save(new Vehicle(vin));
    }

    private Vehicle findOrRegister(final String vin) {
        return find(vin).orElseGet(() -> register(vin));
    }

    public void addSoftwareConfigurations(final String vin, final Collection<String> configurations) {
        addConfiguration(vin, configurations, (configuration) -> configurationService.findOrRegisterSoftwareConfiguration(configuration));
    }

    public void addHardwareConfigurations(final String vin, final Collection<String> configurations) {
        addConfiguration(vin, configurations, (configuration) -> configurationService.findOrRegisterHardwareConfiguration(configuration));
    }

    private void addConfiguration(final String vin, final Collection<String> configurationIds, final Function<String, Configuration> configurationSupplier) {
        final Vehicle vehicle = findOrRegister(vin);
        final Set<Configuration> configurations = configurationIds.stream()
                .map(configurationSupplier)
                .collect(Collectors.toSet());

        vehicle.addConfigurations(configurations);

        repository.save(vehicle);

        compatibilityService.processCompatibility(vehicle);
    }

    public Optional<Vehicle> find(final String vin) {
        return repository.findById(vin);
    }

}
