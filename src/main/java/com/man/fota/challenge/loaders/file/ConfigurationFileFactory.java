package com.man.fota.challenge.loaders.file;

import com.man.fota.challenge.exceptions.UnknownConfigurationFileException;
import org.springframework.util.MultiValueMap;

public class ConfigurationFileFactory {

    private static final String SPLITTER = "_";
    private static final String SOFTWARE = "soft";
    private static final String HARDWARE = "hard";

    public static ConfigurationFile getConfigurationFile(final String fileName, final MultiValueMap<String, String> vinConfigurationMap) {
        final String[] properties = fileName.split(SPLITTER);

        final String type = properties[0];
        final String identifier = properties[1];

        if (type.equalsIgnoreCase(SOFTWARE)) {
            return new SoftwareConfigurationFile(identifier, vinConfigurationMap);
        } else if (type.equalsIgnoreCase(HARDWARE)) {
            return new HardwareConfigurationFile(identifier, vinConfigurationMap);
        } else {
            throw new UnknownConfigurationFileException(fileName);
        }
    }

}
