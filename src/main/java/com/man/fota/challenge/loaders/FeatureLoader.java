package com.man.fota.challenge.loaders;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.man.fota.challenge.services.FeatureService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;

@Component
public class FeatureLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureLoader.class);

    private static final ObjectMapper MAPPER;

    private static final String FEATURE_SPECIFICATIONS_RESOURCE = "features";

    static {
        MAPPER = new ObjectMapper();
    }

    @Autowired
    private FeatureService featureService;

    @PostConstruct
    private void loadFeatures() {
        LOGGER.info("Loading features...");
        final ClassLoader loader = getClass().getClassLoader();
        final URL url = loader.getResource(FEATURE_SPECIFICATIONS_RESOURCE);

        if (url == null) {
            LOGGER.error("Could not load features. Directory {} not found", FEATURE_SPECIFICATIONS_RESOURCE);
            return;
        }

        try {
            if (url.getProtocol().equals("jar")) {
                final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(loader);
                final Resource[] resources = resolver.getResources("classpath*:/" + FEATURE_SPECIFICATIONS_RESOURCE + "/*");

                for (Resource resource : resources) {
                    // let's first get the relative path (feature_X.json)
                    final String[] split = resource.getURL().toString().split("/");
                    final String relativePath = FEATURE_SPECIFICATIONS_RESOURCE + "/" + split[split.length - 1];

                    // and read the content of the file
                    final InputStream stream = loader.getResourceAsStream(relativePath);

                    if (stream == null) {
                        LOGGER.info("Could not load file: {}", relativePath);
                        continue;
                    }

                    final StringWriter writer = new StringWriter();
                    IOUtils.copy(stream, writer, StandardCharsets.UTF_8);
                    processFeature(writer.toString());
                }
            } else {
                // running on IDE
                for (Path path : Files.newDirectoryStream(Paths.get(url.toURI()))) {
                    if (Files.isRegularFile(path)) {
                        processFeature(Files.readString(path));
                    }
                }
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error("Could not load directory: {}", e.getCause().getMessage());
            return;
        }

        LOGGER.info("Finished loading features...");
    }

    private void processFeature(final String content) {
        final JsonNode node;
        try {
            node = MAPPER.readTree(content);
        } catch (IOException e) {
            LOGGER.error("Could not read jSON content: {}", e.getCause().getMessage());
            return;
        }

        final String feature = node.path("feature").asText();

        final JsonNode requirements = node.path("mustInclude");
        final JsonNode exclusions = node.path("mustNotInclude");

        final Collection<String> softwareRequirements = processConfigurations(requirements.path("softwareCodes"));
        final Collection<String> softwareExclusions = processConfigurations(exclusions.path("softwareCodes"));

        final Collection<String> hardwareRequirements = processConfigurations(requirements.path("hardwareCodes"));
        final Collection<String> hardwareExclusions = processConfigurations(exclusions.path("hardwareCodes"));

        featureService.register(feature, softwareRequirements, softwareExclusions, hardwareRequirements, hardwareExclusions);
    }

    private Collection<String> processConfigurations(final JsonNode node) {
        final Collection<String> configurations = new HashSet<>();
        node.elements().forEachRemaining(element -> configurations.add(element.asText()));
        return configurations;
    }

}
