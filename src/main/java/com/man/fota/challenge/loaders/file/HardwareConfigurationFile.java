package com.man.fota.challenge.loaders.file;

import org.springframework.util.MultiValueMap;

public class HardwareConfigurationFile extends BaseConfigurationFile {

    protected HardwareConfigurationFile(final String identifier, final MultiValueMap<String, String> vinConfigurationMap) {
        super(identifier, vinConfigurationMap);
    }

    @Override
    public void accept(ConfigurationFileVisitor visitor) {
        visitor.visit(this);
    }
}
