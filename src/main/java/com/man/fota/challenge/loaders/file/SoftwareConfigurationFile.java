package com.man.fota.challenge.loaders.file;

import org.springframework.util.MultiValueMap;

public class SoftwareConfigurationFile extends BaseConfigurationFile {

    protected SoftwareConfigurationFile(final String identifier, final MultiValueMap<String, String> vinConfigurationMap) {
        super(identifier, vinConfigurationMap);
    }

    @Override
    public void accept(ConfigurationFileVisitor visitor) {
        visitor.visit(this);
    }
}
