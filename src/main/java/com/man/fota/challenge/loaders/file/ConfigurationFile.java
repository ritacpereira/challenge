package com.man.fota.challenge.loaders.file;

import org.springframework.util.MultiValueMap;

/**
 * Created by ritapereira on 29/09/2019
 */
public interface ConfigurationFile {

    String getIdentifier();

    MultiValueMap<String, String> getVinConfigurationMap();

    void accept(ConfigurationFileVisitor visitor);

}
