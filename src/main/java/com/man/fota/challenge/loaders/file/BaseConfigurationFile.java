package com.man.fota.challenge.loaders.file;

import org.springframework.util.MultiValueMap;

public abstract class BaseConfigurationFile implements ConfigurationFile {

    private final String identifier;
    private final MultiValueMap<String, String> vinConfigurationMap;

    protected BaseConfigurationFile(final String identifier, final MultiValueMap<String, String> vinConfigurationMap) {
        this.identifier = identifier;
        this.vinConfigurationMap = vinConfigurationMap;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public MultiValueMap<String, String> getVinConfigurationMap() {
        return vinConfigurationMap;
    }
}
