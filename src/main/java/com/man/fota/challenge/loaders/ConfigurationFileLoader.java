package com.man.fota.challenge.loaders;

import com.man.fota.challenge.exceptions.LoadingException;
import com.man.fota.challenge.loaders.file.ConfigurationFile;
import com.man.fota.challenge.loaders.file.ConfigurationFileFactory;
import com.man.fota.challenge.loaders.file.ConfigurationFileVisitor;
import com.man.fota.challenge.services.VehicleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Properties;

@Component
@Profile("!test")
public class ConfigurationFileLoader implements ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationFileLoader.class);

    private static final String PROPERTIES_FILE = "application.properties";
    private static final String PATH_PROPERTY = "configurations.file.path";

    private Path path;

    private WatchService watcher;

    @Autowired
    private VehicleService vehicleService;

    private void init() throws LoadingException, IOException {
        watcher = FileSystems.getDefault().newWatchService();

        final Properties properties = new Properties();
        final InputStream stream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE);

        if (stream == null) {
            throw new LoadingException();
        }
        properties.load(stream);

        this.path = Paths.get(properties.getProperty(PATH_PROPERTY));
        path.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);
        LOGGER.info("Watch service registered for directory {}", path);
    }

    @Override
    public void run(ApplicationArguments args) throws InterruptedException {
        try {
            init();
        } catch (IOException e) {
            LOGGER.error("Could not initialize watch service to load configurations");
            return;
        } catch (LoadingException e) {
            LOGGER.error("Could not load properties file to read configurations folder path");
            return;
        }

        LOGGER.info("Watching folder {}...", path);
        try {
            for (Path filePath : Files.newDirectoryStream(path)) {
                if (validFile(filePath)) {
                    processConfiguration(filePath);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error reading existing file. Continuing...");
        }

        WatchKey key;
        while ((key = watcher.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                processConfiguration(event.context().toString());
            }
            key.reset();
        }
    }

    private boolean validFile(Path filePath) {
        return Files.isRegularFile(filePath)
                && (filePath.toString().contains("hard") || filePath.toString().contains("soft"));
    }

    private void processConfiguration(final Path filePath) {
        processConfiguration(filePath.getFileName().toString());
    }

    private void processConfiguration(final String fileName) {
        final String filePath = path + "/" + fileName;
        LOGGER.info("Processing configuration {}", filePath);

        final BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            LOGGER.error("Could not process configuration. File {} not found.", filePath);
            return;
        }

        final MultiValueMap<String, String> vinConfigurationMap = new LinkedMultiValueMap<>();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                final String[] data = line.split(",");
                vinConfigurationMap.add(data[0], data[1]);
            }
        } catch (IOException e) {
            LOGGER.error("Error trying to read file {}", filePath);
            return;
        }

        final ConfigurationFile file = ConfigurationFileFactory.getConfigurationFile(removeExtension(fileName), vinConfigurationMap);
        file.accept(new ConfigurationFileVisitor(vehicleService));
        LOGGER.info("Finished processing configuration {}", filePath);
    }

    private String removeExtension(String fileName) {
        return fileName.split("\\.")[0];
    }
}
