package com.man.fota.challenge.loaders.file;

import com.man.fota.challenge.services.VehicleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class ConfigurationFileVisitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationFileVisitor.class);

    private static final Set<String> visitedHardwareFiles;
    private static final Set<String> visitedSoftwareFiles;

    private VehicleService vehicleService;

    static {
        visitedHardwareFiles = new HashSet<>();
        visitedSoftwareFiles = new HashSet<>();
    }

    public ConfigurationFileVisitor(final VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    public void visit(final HardwareConfigurationFile file) {
        if (visitedHardwareFiles.contains(file.getIdentifier())) {
            LOGGER.warn("Hardware configuration file {} was already processed. Bypassing.", file.getIdentifier());
            return;
        }

        visitedHardwareFiles.add(file.getIdentifier());
        file.getVinConfigurationMap().forEach((key, value) -> vehicleService.addHardwareConfigurations(key, value));
    }

    public void visit(final SoftwareConfigurationFile file) {
        if (visitedSoftwareFiles.contains(file.getIdentifier())) {
            LOGGER.warn("Software configuration file {} was already processed. Bypassing.", file.getIdentifier());
            return;
        }

        visitedSoftwareFiles.add(file.getIdentifier());
        file.getVinConfigurationMap().forEach((key, value) -> vehicleService.addSoftwareConfigurations(key, value));
    }

}
