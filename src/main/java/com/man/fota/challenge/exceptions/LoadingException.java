package com.man.fota.challenge.exceptions;

public class LoadingException extends Exception {

    private final static String ERROR_MESSAGE = "Could not load configuration properties file";

    public LoadingException() {
        super(ERROR_MESSAGE);
    }

}
