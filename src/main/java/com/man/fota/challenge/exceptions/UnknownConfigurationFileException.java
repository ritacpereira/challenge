package com.man.fota.challenge.exceptions;

public class UnknownConfigurationFileException extends RuntimeException {

    private final static String ERROR_MESSAGE = "Could not load configuration file %s. Unknown configuration type";

    public UnknownConfigurationFileException(final String fileName) {
        super(String.format(ERROR_MESSAGE, fileName));
    }

}
