package com.man.fota.challenge.utils;

import java.util.Collections;
import java.util.Set;

public final class TestDefaultValues {

    private static final String RSC = "%s#RSC#%d";
    private static final String ESC = "%s#ESC#%d";
    private static final String RHC = "%s#RHC#%d";
    private static final String EHC = "%s#EHC#%d";

    private static String requiredSoftwareCode(final String feature, final int index) {
        return String.format(RSC, feature, index);
    }

    private static String excludedSoftwareCode(final String feature, final int index) {
        return String.format(ESC, feature, index);
    }

    private static String requiredHardwareCode(final String feature, final int index) {
        return String.format(RHC, feature, index);
    }

    private static String excludedHardwareCode(final String feature, final int index) {
        return String.format(EHC, feature, index);
    }

    public static final String FEATURE_A = "A";
    public static final String FEATURE_B = "B";
    public static final String FEATURE_C = "C";

    public static final String A_RSC_1 = requiredSoftwareCode(FEATURE_A, 1);
    public static final String A_RSC_2 = requiredSoftwareCode(FEATURE_A, 2);

    public static final String A_ESC_1 = excludedSoftwareCode(FEATURE_A, 1);

    public static final String A_RHC_1 = requiredHardwareCode(FEATURE_A, 1);
    public static final String A_RHC_2 = requiredHardwareCode(FEATURE_A, 2);
    public static final String A_RHC_3 = requiredHardwareCode(FEATURE_A, 3);

    public static final String A_EHC_1 = excludedHardwareCode(FEATURE_A, 1);
    public static final String A_EHC_2 = excludedHardwareCode(FEATURE_A, 2);
    public static final String A_EHC_3 = excludedHardwareCode(FEATURE_A, 3);

    public static final String B_RSC_1 = requiredSoftwareCode(FEATURE_B, 1);

    public static final String B_RHC_1 = requiredHardwareCode(FEATURE_B, 1);
    public static final String B_RHC_2 = requiredHardwareCode(FEATURE_B, 2);
    public static final String B_RHC_3 = requiredHardwareCode(FEATURE_B, 3);

    public static final String C_EHC_1 = excludedHardwareCode(FEATURE_C, 1);

    public static final Set<String> A_RSC_SET = Set.of(A_RSC_1, A_RSC_2);
    public static final Set<String> A_ESC_SET = Set.of(A_ESC_1);
    public static final Set<String> A_RHC_SET = Set.of(A_RHC_1, A_RHC_2, A_RHC_3);
    public static final Set<String> A_EHC_SET = Set.of(A_EHC_1, A_EHC_2, A_EHC_3);

    public static final Set<String> B_RSC_SET = Set.of(B_RSC_1);
    public static final Set<String> B_ESC_SET = Collections.emptySet();
    public static final Set<String> B_RHC_SET = Set.of(B_RHC_1, B_RHC_2, B_RHC_3);
    public static final Set<String> B_EHC_SET = Collections.emptySet();

    public static final Set<String> C_RSC_SET = Collections.emptySet();
    public static final Set<String> C_ESC_SET = Collections.emptySet();
    public static final Set<String> C_RHC_SET = Collections.emptySet();
    public static final Set<String> C_EHC_SET = Set.of(C_EHC_1);

    public static final String VIN_X = "X";

}
