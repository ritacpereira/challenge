package com.man.fota.challenge.controllers;

import com.man.fota.challenge.services.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers.openApi;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class FeatureControllerTest {

    private static final String SPEC_URL = "/api/swagger-coding-challenge-fota.yaml";

    private static final String VIN = "X";
    private static final String VEHICLE_JSON = "[{\"vin\":\"" + VIN + "\",\"configurations\":[]}]";

    @Autowired
    private FeatureController featureController;

    @Autowired
    private VehicleService vehicleService;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        this.mvc = MockMvcBuilders.standaloneSetup(featureController).build();
    }

    @Test
    public void testValidGetAll() throws Exception {
        mvc.perform(get("/fota/features/"))
                .andExpect(status().isOk())
                .andExpect(openApi().isValid(SPEC_URL));
    }

    @Test
    public void testValidGetVinsByFeature() throws Exception {
        mvc.perform(get("/fota/features/A"))
                .andExpect(status().isOk())
                .andExpect(openApi().isValid(SPEC_URL));
    }

    @Test
    public void testInvalidGetVinsByFeature() throws Exception {
        mvc.perform(get("/fota/features/1"))
                .andExpect(status().isNotFound())
                .andExpect(openApi().isValid(SPEC_URL));
    }

    @Test
    public void testValidEmptyInstallableVinsByFeatures() throws Exception {
        mvc.perform(get("/fota/features/A/installable"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"))
                .andExpect(openApi().isValid(SPEC_URL));
    }

    @Test
    public void testValidNonEmptyIncompatibleVinsByFeature() throws Exception {
        vehicleService.addSoftwareConfigurations(VIN, Collections.emptyList());

        mvc.perform(get("/fota/features/A/incompatible"))
                .andExpect(status().isOk())
                .andExpect(content().string(VEHICLE_JSON))
                .andExpect(openApi().isValid(SPEC_URL));
    }

}
