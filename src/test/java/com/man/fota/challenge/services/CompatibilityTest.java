package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Feature;
import com.man.fota.challenge.repositories.FeatureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.stream.Collectors;

import static com.man.fota.challenge.utils.TestDefaultValues.A_EHC_2;
import static com.man.fota.challenge.utils.TestDefaultValues.A_EHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.A_ESC_1;
import static com.man.fota.challenge.utils.TestDefaultValues.A_ESC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RHC_1;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RHC_2;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RHC_3;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RSC_1;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RSC_2;
import static com.man.fota.challenge.utils.TestDefaultValues.A_RSC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.B_EHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.B_ESC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.B_RHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.B_RSC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.C_EHC_1;
import static com.man.fota.challenge.utils.TestDefaultValues.C_EHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.C_ESC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.C_RHC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.C_RSC_SET;
import static com.man.fota.challenge.utils.TestDefaultValues.FEATURE_A;
import static com.man.fota.challenge.utils.TestDefaultValues.FEATURE_B;
import static com.man.fota.challenge.utils.TestDefaultValues.FEATURE_C;
import static com.man.fota.challenge.utils.TestDefaultValues.VIN_X;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class CompatibilityTest {

    @Autowired
    private FeatureService featureService;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private VehicleService vehicleService;

    @BeforeEach
    public void cleanRepositories() {
        featureRepository.deleteAll();
    }

    @Test
    public void testFullCompatibility() {
        featureService.register(FEATURE_A, A_RSC_SET, A_ESC_SET, A_RHC_SET, A_EHC_SET);
        featureService.register(FEATURE_B, B_RSC_SET, B_ESC_SET, B_RHC_SET, B_EHC_SET);
        featureService.register(FEATURE_C, C_RSC_SET, C_ESC_SET, C_RHC_SET, C_EHC_SET);

        vehicleService.register(VIN_X);
        vehicleService.addSoftwareConfigurations(VIN_X, A_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, A_RHC_SET);
        vehicleService.addSoftwareConfigurations(VIN_X, B_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, B_RHC_SET);
        vehicleService.addSoftwareConfigurations(VIN_X, C_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, C_RHC_SET);

        assertThat(featureService.getCompatible(VIN_X).stream()
                .map(Feature::getId)
                .collect(Collectors.toSet()))
                .containsExactlyInAnyOrder(FEATURE_A, FEATURE_B, FEATURE_C);
        assertThat(featureService.getIncompatible(VIN_X)).isEmpty();
    }

    @Test
    public void testPartialCompatibility() {
        featureService.register(FEATURE_A, A_RSC_SET, A_ESC_SET, A_RHC_SET, A_EHC_SET);
        featureService.register(FEATURE_B, B_RSC_SET, B_ESC_SET, B_RHC_SET, B_EHC_SET);
        featureService.register(FEATURE_C, C_RSC_SET, C_ESC_SET, C_RHC_SET, C_EHC_SET);

        vehicleService.register(VIN_X);
        vehicleService.addSoftwareConfigurations(VIN_X, A_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, A_RHC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, Collections.singleton(C_EHC_1));

        assertThat(featureService.getCompatible(VIN_X).stream()
                .map(Feature::getId)
                .collect(Collectors.toSet()))
                .containsExactlyInAnyOrder(FEATURE_A);
        assertThat(featureService.getIncompatible(VIN_X).stream()
                .map(Feature::getId)
                .collect(Collectors.toSet()))
                .containsExactlyInAnyOrder(FEATURE_B, FEATURE_C);
    }

    @Test
    public void testNoCompatibility() {
        featureService.register(FEATURE_A, A_RSC_SET, A_ESC_SET, A_RHC_SET, A_EHC_SET);
        featureService.register(FEATURE_B, B_RSC_SET, B_ESC_SET, B_RHC_SET, B_EHC_SET);
        featureService.register(FEATURE_C, C_RSC_SET, C_ESC_SET, C_RHC_SET, C_EHC_SET);

        vehicleService.register(VIN_X);
        vehicleService.addSoftwareConfigurations(VIN_X, A_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, A_RHC_SET);
        // missing software requirements for feature B
        vehicleService.addHardwareConfigurations(VIN_X, B_RHC_SET);
        vehicleService.addSoftwareConfigurations(VIN_X, C_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, C_RHC_SET);
        vehicleService.addSoftwareConfigurations(VIN_X, Collections.singleton(A_EHC_2));
        vehicleService.addHardwareConfigurations(VIN_X, Collections.singleton(C_EHC_1));

        assertThat(featureService.getCompatible(VIN_X)).isEmpty();
        assertThat(featureService.getIncompatible(VIN_X).stream()
                .map(Feature::getId)
                .collect(Collectors.toSet()))
                .containsExactlyInAnyOrder(FEATURE_A, FEATURE_B, FEATURE_C);
    }

    @Test
    public void testAddCompatibility() {
        featureService.register(FEATURE_A, A_RSC_SET, A_ESC_SET, A_RHC_SET, A_EHC_SET);
        vehicleService.register(VIN_X);
        vehicleService.addSoftwareConfigurations(VIN_X, Collections.singleton(A_RSC_1));
        vehicleService.addSoftwareConfigurations(VIN_X, Collections.singleton(A_RSC_2));
        vehicleService.addHardwareConfigurations(VIN_X, Collections.singleton(A_RHC_1));
        vehicleService.addHardwareConfigurations(VIN_X, Collections.singleton(A_RHC_2));
        // missing requirement constraint

        assertThat(featureService.getCompatible(VIN_X)).isEmpty();
        assertThat(featureService.getIncompatible(VIN_X)).hasSize(1);

        vehicleService.addHardwareConfigurations(VIN_X, Collections.singleton(A_RHC_3));

        assertThat(featureService.getCompatible(VIN_X)).hasSize(1);
        assertThat(featureService.getIncompatible(VIN_X)).isEmpty();
    }

    @Test
    public void testBreakCompatibility() {
        featureService.register(FEATURE_A, A_RSC_SET, A_ESC_SET, A_RHC_SET, A_EHC_SET);
        vehicleService.register(VIN_X);
        vehicleService.addSoftwareConfigurations(VIN_X, A_RSC_SET);
        vehicleService.addHardwareConfigurations(VIN_X, A_RHC_SET);

        assertThat(featureService.getCompatible(VIN_X)).hasSize(1);
        assertThat(featureService.getIncompatible(VIN_X)).isEmpty();

        vehicleService.addSoftwareConfigurations(VIN_X, Collections.singleton(A_ESC_1));

        assertThat(featureService.getCompatible(VIN_X)).isEmpty();
        assertThat(featureService.getIncompatible(VIN_X)).hasSize(1);
    }

}
