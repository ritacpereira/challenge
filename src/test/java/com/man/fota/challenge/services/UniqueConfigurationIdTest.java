package com.man.fota.challenge.services;

import com.man.fota.challenge.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class UniqueConfigurationIdTest {

    private static final String VIN = "VIN";
    private static final String CONFIGURATION_ID = "CONFIGURATION";
    private static final String DIFFERENT_CONFIGURATION_ID = "NEW_CONFIGURATION";

    @Autowired
    private VehicleService vehicleService;

    @BeforeEach
    public void initTest() {
        vehicleService.register(VIN);
        vehicleService.addSoftwareConfigurations(VIN, Collections.singleton(CONFIGURATION_ID));
    }

    @Test
    public void testAddNewConfiguration() {
        final Vehicle vehicle = vehicleService.find(VIN).orElseThrow();
        assertThat(vehicle.getConfigurations())
                .withFailMessage("Vehicle %s should have 1 configuration but has %s", VIN, vehicle.getConfigurations().size())
                .hasSize(1);
        vehicle.getConfigurations().forEach(configuration -> assertThat(configuration.getId())
                .withFailMessage("Expected id %s but found %s", CONFIGURATION_ID, configuration.getId())
                .isEqualTo(CONFIGURATION_ID));
    }

    @Test
    public void testDuplicateConfigurationId() {
        vehicleService.addHardwareConfigurations(VIN, Collections.singleton(CONFIGURATION_ID));

        final Vehicle vehicle = vehicleService.find(VIN).orElseThrow();
        assertThat(vehicle.getConfigurations())
                .withFailMessage("Vehicle %s should have 1 configuration but has %s", VIN, vehicle.getConfigurations().size())
                .hasSize(1);
        vehicle.getConfigurations().forEach(configuration -> assertThat(configuration.getId())
                .withFailMessage("Expected id %s but found %s", CONFIGURATION_ID, configuration.getId())
                .isEqualTo(CONFIGURATION_ID)
        );
    }

    @Test
    public void testDifferentConfigurationId() {
        vehicleService.addHardwareConfigurations(VIN, Collections.singleton(DIFFERENT_CONFIGURATION_ID));

        final Vehicle vehicle = vehicleService.find(VIN).orElseThrow();
        assertThat(vehicle.getConfigurations())
                .withFailMessage("Vehicle %s should have 2 configuration but has %s", VIN, vehicle.getConfigurations().size())
                .hasSize(2);
    }

}
