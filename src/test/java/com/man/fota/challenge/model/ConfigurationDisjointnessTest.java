package com.man.fota.challenge.model;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigurationDisjointnessTest {

    private static final Configuration REQUIREMENT_X = Configuration.softwareConfiguration("REQUIREMENT_X");
    private static final Configuration REQUIREMENT_Y = Configuration.softwareConfiguration("REQUIREMENT_Y");
    private static final Configuration REQUIREMENT_Z = Configuration.hardwareConfiguration("REQUIREMENT_Z");

    private static final Configuration EXCLUSION_X = Configuration.softwareConfiguration("EXCLUSION_X");
    private static final Configuration EXCLUSION_Y = Configuration.hardwareConfiguration("EXCLUSION_Y");
    private static final Configuration EXCLUSION_Z = Configuration.hardwareConfiguration("EXCLUSION_Z");

    private static final String FEATURE_ID = "A";

    @Test
    public void testConfigurationsDisjointness() {
        final Set<Configuration> requirements = Set.of(REQUIREMENT_X, REQUIREMENT_Y, REQUIREMENT_Z);
        final Set<Configuration> exclusions = Set.of(EXCLUSION_X, EXCLUSION_Y, EXCLUSION_Z);
        final Feature feature = new Feature(FEATURE_ID);
        requirements.forEach(feature::addRequirement);
        exclusions.forEach(feature::addExclusion);

        assertThat(feature.getRequirements()).containsExactlyInAnyOrderElementsOf(requirements);
        assertThat(feature.getExclusions()).containsExactlyInAnyOrderElementsOf(exclusions);
    }

    @Test
    public void testConfigurationsOverlap() {
        final Set<Configuration> requirements = Set.of(REQUIREMENT_X, REQUIREMENT_Y, REQUIREMENT_Z);
        final Set<Configuration> exclusions = Set.of(EXCLUSION_X, EXCLUSION_Y, EXCLUSION_Z, REQUIREMENT_X);
        final Feature feature = new Feature(FEATURE_ID);
        requirements.forEach(feature::addRequirement);
        exclusions.forEach(feature::addExclusion);

        assertThat(feature.getRequirements()).containsExactlyInAnyOrderElementsOf(requirements);
        assertThat(feature.getExclusions())
                .withFailMessage("Requirement %s should not be in constraints list", REQUIREMENT_X)
                .doesNotContain(REQUIREMENT_X);
    }

}
